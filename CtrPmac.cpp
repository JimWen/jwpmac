#include <stdafx.h>
#include "CtrHeader.h"
#include "CtrPmac.h"

CCtrPmac::CCtrPmac(const UINT uCardNum/*=0*/)
{
	m_uCardNum = uCardNum;

	m_uMaxChar = 255;
}

CCtrPmac::~CCtrPmac()
{
	
}

/**
 *功能:向PMAC发送ASCII字符串指令，不需要接受
 *参数:szCommand--以NULL结尾长度大小为255的包含ASCII指令字符串的字符数组
 *返回:参见PCOMM32手册中PmacGetResponseEx的返回值
 *其他:2013/11/19 By Jim Wen Ver1.0
**/
int CCtrPmac::GiveCommand(LPCTSTR szCommand)
{
	return PmacGetResponseEx(m_uCardNum, m_szResponse, m_uMaxChar, szCommand);
}

/**
 *功能:向PMAC发送ASCII字符串指令，需要接受
 *参数:szCommand--以NULL结尾长度大小为255的包含ASCII指令字符串的字符数组
 *     szReponse--以NULL结尾长度大小为255的用于接受返回ASCII字符串的字符数组
 *返回:参见PCOMM32手册中PmacGetResponseEx的返回值
 *其他:2013/11/19 By Jim Wen Ver1.0
**/
int CCtrPmac::GetResponse( LPTSTR szResponse, LPCTSTR szCommand)
{
	return PmacGetResponseEx(m_uCardNum, szResponse, m_uMaxChar, szCommand);
}

/**
 *功能:获得PMAC的short类型的参数值
 *参数:cType--参数类型，取值为'I','P','Q','M'
 *     nIndex--对应变量的序号值
 *	   snDef--默认值,为0
 *返回:对应参数值，出问题时返回默认值
 *其他:2013/11/19 By Jim Wen Ver1.0
**/
short int CCtrPmac::GetShort( const char cType, const UINT nIndex, const short int snDef/*=0*/ )
{
	return PmacGetVariable(m_uCardNum, cType, nIndex, snDef);
}

/**
 *功能:获得PMAC的long类型的参数值
 *参数:cType--参数类型，取值为'I','P','Q','M'
 *     nIndex--对应变量的序号值
 *	   lnDef--默认值,为0
 *返回:对应参数值，出问题时返回默认值
 *其他:2013/11/19 By Jim Wen Ver1.0
**/
long CCtrPmac::GetLong( const char cType, const UINT nIndex, const long lnDef/*=0*/ )
{
	return PmacGetVariableLong(m_uCardNum, cType, nIndex, lnDef);
}

/**
 *功能:获得PMAC的double类型的参数值
 *参数:cType--参数类型，取值为'I','P','Q','M'
 *     nIndex--对应变量的序号值
 *	   dbDef--默认值,为0
 *返回:对应参数值，出问题时返回默认值
 *其他:2013/11/19 By Jim Wen Ver1.0
**/
double CCtrPmac::GetDouble( const char cType, const UINT nIndex, const double dbDef/*=0*/ )
{
	return PmacGetVariableDouble(m_uCardNum, cType, nIndex, dbDef);
}

/**
 *功能:设置PMAC的short类型的参数值
 *参数:cType--参数类型，取值为'I','P','Q','M'
 *     nIndex--对应变量的序号值
 *	   snValSet--待设定的参数值
 *返回:无
 *其他:2013/11/19 By Jim Wen Ver1.0
**/
void CCtrPmac::SetShort( const char cType, const UINT nIndex, const short int snValSet )
{
	PmacSetVariable(m_uCardNum, cType, nIndex, snValSet);
}

/**
 *功能:设置PMAC的long类型的参数值
 *参数:cType--参数类型，取值为'I','P','Q','M'
 *     nIndex--对应变量的序号值
 *	   lnValSet--待设定的参数值
 *返回:无
 *其他:2013/11/19 By Jim Wen Ver1.0
**/
void CCtrPmac::SetLong( const char cType, const UINT nIndex, const long lnValSet )
{
	PmacSetVariableLong(m_uCardNum, cType, nIndex, lnValSet);
}

/**
 *功能:设置PMAC的double类型的参数值
 *参数:cType--参数类型，取值为'I','P','Q','M'
 *     nIndex--对应变量的序号值
 *	   dbValSet--待设定的参数值
 *返回:无
 *其他:2013/11/19 By Jim Wen Ver1.0
**/
void CCtrPmac::SetDouble( const char cType, const UINT nIndex, const double dbValSet )
{
	PmacSetVariableDouble(m_uCardNum, cType, nIndex, dbValSet);
}


/**
 *功能:向PMAC中DOWN入单个文件
 *参数:szFileName--要Down的文件路径名，以NULL结尾字符串
 *返回:Down成功为TRUE，否则为FALSE
 *其他:2013/11/20 By Jim Wen Ver1.0
**/
BOOL CCtrPmac::DownloadFile(PCHAR szFileName )
{
	return 0 == PmacDownload(m_uCardNum, NULL, NULL, NULL, szFileName, FALSE, FALSE, FALSE, TRUE) ? FALSE : TRUE;
}
