#include <stdafx.h>
#include "CtrCenter.h"

CCtrCenter::CCtrCenter()
{
	m_hPmacLink			=  NULL;
	m_pPmac				=  NULL;
	m_pMotorDownX		=  NULL;
	m_pMotorDownY		=  NULL;
	m_pMotorUpX			=  NULL;
	m_pMotorUpY			=  NULL;
	m_pMotorUpZ			=  NULL;
	m_pProgDownXHome	=  NULL;
	m_pProgDownYHome	=  NULL;
	m_pParam			=  NULL;
	m_pIO				=  NULL;

	m_bIsInit = FALSE;
}

CCtrCenter::~CCtrCenter()
{
	//删除PMAC卡对象
	if(NULL != m_pIO)
	{
		delete m_pIO;
	}
	if(NULL != m_pParam)
	{
		delete m_pParam;
	}
	if(NULL != m_pProgDownXHome)
	{
		delete m_pProgDownXHome;
	}
	if(NULL != m_pProgDownYHome)
	{
		delete m_pProgDownYHome;
	}
	if(NULL != m_pMotorUpZ)
	{
		delete m_pMotorUpZ;
	}
	if(NULL != m_pMotorUpY)
	{
		delete m_pMotorUpY;
	}
	if(NULL != m_pMotorUpX)
	{
		delete m_pMotorUpX;
	}
	if(NULL != m_pMotorDownY)
	{
		delete m_pMotorDownY;
	}
	if(NULL != m_pMotorDownX)
	{
		delete m_pMotorDownX;
	}

	//关闭和停止链接PMAC
	if (NULL != m_hPmacLink)
	{
		CloseRuntimeLink(CARD_NUM);
	}
}

BOOL CCtrCenter::InitialControl(PMACINTRPROC pmacIntProc/*=CCtrCenter::InterruptFunc*/)
{
	//链接和打开PMAC
	if(NULL != (m_hPmacLink = PmacRuntimeLink(CARD_NUM)))
	{
		//创建PMAC卡对象,必须最先创建,其他的对象依靠此
		m_pPmac = new CCtrPmac;

		//创建PMAC各个对象
		m_pMotorDownX			= new CCtrMotor(6, m_pPmac);
		m_pMotorDownY			= new CCtrMotor(8, m_pPmac);
		m_pMotorUpX				= new CCtrMotor(3, m_pPmac);
		m_pMotorUpY				= new CCtrMotor(4, m_pPmac);
		m_pMotorUpZ				= new CCtrMotor(5, m_pPmac);
		m_pProgDownXHome		= new CCtrProg(1, 6, m_pPmac);
		m_pProgDownYHome		= new CCtrProg(2, 8, m_pPmac);
		m_pParam				= new CCtrParam(m_pPmac);
		m_pIO					= new CCtrIO(m_pPmac);

		//初始化回调函数
		m_bIsInit = PmacINTRFuncCallInit(CARD_NUM, pmacIntProc, WM_PMAC_INTERRUPT, 0xFF1F);
		return  m_bIsInit;
	}
	else
	{
		return FALSE;
	}
}

void WINAPI CCtrCenter::InterruptFunc( DWORD msg, PINTRBUFFER pBuffer )
{
	
	//这里的ISR_IR6为中断响应通道，根据PMAC不同的跳线设置而不同
	if (pBuffer->dwInterruptType == ISR_IR6)
	{

	}
}
