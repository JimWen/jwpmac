#include <stdafx.h>
#include "CtrHeader.h"
#include "CtrPmac.h"
#include "CtrProg.h"

CCtrProg::CCtrProg(const UINT uAxisIndex, const UINT uProgIndex, CCtrPmac *pCtrPmac)
{
	m_uAxisIndex = uAxisIndex;
	m_uProgIndex = uProgIndex;
	m_pCtrPmac = pCtrPmac;
}

CCtrProg::CCtrProg( const UINT uPLCIndex, CCtrPmac *pCtrPmac )
{
	m_uPLCIndex = uPLCIndex;
	m_pCtrPmac = pCtrPmac;
}

CCtrProg::~CCtrProg()
{
	
}

/**
 *功能:执行指定轴号的指定号运动程序运行
 *参数:无
 *返回:无
 *其他:2013/11/19 By Jim Wen Ver1.0
**/
void CCtrProg::Run()
{
	_stprintf(m_szCommand, TEXT("&%db%dr"), m_uAxisIndex, m_uProgIndex);
	m_pCtrPmac->GiveCommand(m_szCommand);
}

/**
 *功能:停止指定轴号的指定号运动程序运行
 *参数:无
 *返回:无
 *其他:2013/11/19 By Jim Wen Ver1.0
**/
void CCtrProg::Stop()
{
	_stprintf(m_szCommand, TEXT("&%db%da"), m_uAxisIndex, m_uProgIndex);//abort 程序执行
	m_pCtrPmac->GiveCommand(m_szCommand);	
}

/**
 *功能:指定号PLC程序执行
 *参数:无
 *返回:无
 *其他:2013/11/19 By Jim Wen Ver1.0
**/
void CCtrProg::Enable()
{
	_stprintf(m_szCommand, TEXT("enable plc %d"), m_uPLCIndex);
	m_pCtrPmac->GiveCommand(m_szCommand);	
}

/**
 *功能:指定号PLC程序停止执行
 *参数:无
 *返回:无
 *其他:2013/11/19 By Jim Wen Ver1.0
**/
void CCtrProg::Disable()
{
	_stprintf(m_szCommand, TEXT("disable plc %d"), m_uPLCIndex);
	m_pCtrPmac->GiveCommand(m_szCommand);	
}

/**
 *功能:向PMAC中DOWN入单个文件
 *参数:szFileName--要Down的文件路径名，以NULL结尾字符串
 *返回:Down成功为TRUE，否则为FALSE
 *其他:2013/11/20 By Jim Wen Ver1.0
**/
BOOL CCtrProg::Download( PCHAR szFileName )
{
	return m_pCtrPmac->DownloadFile(szFileName);
}

