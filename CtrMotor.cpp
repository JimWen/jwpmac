#include <stdafx.h>
#include <stdio.h>
#include "CtrHeader.h"
#include "CtrPmac.h"
#include "CtrMotor.h"

/**
 *功能:构造函数
 *参数:uIndex--电机编号
	   pCtrPmac--PMAC对象
	   uOneMmCts--电机运动一个Cts对应的脉冲数,默认为819.2
	   uCycleTime--电机的一个伺服周期,默认为442 us
 *返回:无
 *其他:2014/01/13 By 文洲 Ver1.0
**/
CCtrMotor::CCtrMotor( const UINT uIndex, 
					  CCtrPmac *pCtrPmac, 
					  const UINT uOneMmCts/*=819.2*/, 
					  const UINT uCycleTime/*=442*/)
{
	m_pCtrPmac = pCtrPmac;
	m_uIndex = uIndex;
	m_uOneMmCts = uOneMmCts;
	m_uCycleTime = uCycleTime;
}

CCtrMotor::~CCtrMotor()
{
	
}

/**
 *功能:使能电机
 *参数:无
 *返回:无
 *其他:2013/11/19 By Jim Wen Ver1.0
**/
void CCtrMotor::Enable()
{
	_stprintf(m_szCommand, TEXT("#%dj/"), m_uIndex);
	m_pCtrPmac->GiveCommand(m_szCommand);
}

/**
 *功能:禁能电机
 *参数:无
 *返回:无
 *其他:2013/11/19 By Jim Wen Ver1.0
**/
void CCtrMotor::Disable()
{
	_stprintf(m_szCommand, TEXT("#%dk"), m_uIndex);
	m_pCtrPmac->GiveCommand(m_szCommand);	
}

/**
 *功能:对应电机轴回零,实际应用中估计用途不大，主要是通过回零程序来完成回零
 *参数:无
 *返回:无
 *其他:2013/11/19 By Jim Wen Ver1.0
**/
void CCtrMotor::Home()
{
	
	_stprintf(m_szCommand, TEXT("homez %d"), m_uIndex);
	m_pCtrPmac->GiveCommand(m_szCommand);	
}

/**
 *功能:电机jog运动到指定位置
 *参数:dPosition--指定电机位置,单位为mm
 *返回:无
 *其他:2013/11/29 By Jim Wen Ver1.0
**/
void CCtrMotor::JogMoveTo(const double dPosition)
{
	_stprintf(m_szCommand, TEXT("#%dj=%f"), m_uIndex, dPosition*m_uOneMmCts);
	TRACE(m_szCommand);
	m_pCtrPmac->GiveCommand(m_szCommand);
}

/**
 *功能:电机jog从当前实际位置运动指定距离
 *参数:dDisatance--要运动的指定距离,单位为mm
 *返回:无
 *其他:2013/11/29 By Jim Wen Ver1.0
**/
void CCtrMotor::JogMove(  const double dDisatance )
{
	_stprintf(m_szCommand, TEXT("#%dj^%f"), m_uIndex, dDisatance*m_uOneMmCts);
	TRACE(m_szCommand);
	m_pCtrPmac->GiveCommand(m_szCommand);
}

/**
 *功能:设置电机jog速度
 *参数:dSpeed--要设置的jog速度,单位mm/s
 *返回:无
 *其他:2013/11/29 By Jim Wen Ver1.0
**/
void CCtrMotor::SetJogSpeed( const double dSpeed )
{
	_stprintf(m_szCommand, TEXT("I%d22=%f"), m_uIndex, dSpeed*m_uOneMmCts);
	m_pCtrPmac->GiveCommand(m_szCommand);
}

/**
 *功能:设置电机jog总的加速时间
 *参数:lnTime--要设置的总加速时间,单位ms
 *返回:无
 *其他:2013/11/29 By Jim Wen Ver1.0
**/
void CCtrMotor::SetJogAccelTime( const long lnTime )
{
	_stprintf(m_szCommand, TEXT("I%d20=%ld"), m_uIndex, lnTime);
	m_pCtrPmac->GiveCommand(m_szCommand);
}

/**
 *功能:设置电机jog S曲线加速时间
 *参数:lnTime--要设置的S曲线加速时间,单位ms
 *返回:无
 *其他:2013/11/29 By Jim Wen Ver1.0
**/
void CCtrMotor::SetJogSAccelTime( const long lnTime )
{
	_stprintf(m_szCommand, TEXT("I%d21=%ld"), m_uIndex, lnTime);
	m_pCtrPmac->GiveCommand(m_szCommand);
}

/**
 *功能:设置电机当前实际位置
 *参数:无
 *返回:电机当前实际位置,单位mm
 *其他:2013/11/29 By Jim Wen Ver1.0
**/
double CCtrMotor::GetCurPosition(  )
{
	return m_pCtrPmac->GetDouble('M', m_uIndex*100+62) / 
		   (m_pCtrPmac->GetDouble('I', m_uIndex*100+8) * 32 * m_uOneMmCts);
}

/**
 *功能:设置电机当前实际速度
 *参数:无
 *返回:电机当前实际速度,单位mm/s
 *其他:2013/11/29 By Jim Wen Ver1.0
**/
double CCtrMotor::GetCurSpeed(  )
{
	return m_pCtrPmac->GetDouble('M', m_uIndex*100+66) / 
		   (m_pCtrPmac->GetDouble('I', m_uIndex*100+9) * 32 * m_uCycleTime/1000000 * m_uOneMmCts);
}
