#ifndef CTRMOTOR_H_H_H
#define CTRMOTOR_H_H_H

class CONTROL_DLL CCtrMotor
{
public:
	CCtrMotor(const UINT uIndex, CCtrPmac *pCtrPmac, const UINT uOneMmCts=819.2, const UINT uCycleTime=442);
	virtual ~CCtrMotor();

	void Enable();
	void Disable(); 
	void JogMoveTo(const double dPosition);
	void JogMove(const double dDisatance);
	void SetJogSpeed(const double dSpeed);
	void SetJogAccelTime(const long lnTime);
	void SetJogSAccelTime(const long lnTime);
	double GetCurPosition();
	double GetCurSpeed();
	void Home();

private:
	CCtrPmac *m_pCtrPmac;
	UINT m_uIndex;
	UINT m_uOneMmCts;
	UINT m_uCycleTime;
	TCHAR m_szCommand[255];
};

#endif