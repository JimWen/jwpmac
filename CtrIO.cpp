#include <stdafx.h>
#include "CtrHeader.h"
#include "CtrPmac.h"
#include "CtrIO.h"

CCtrIO::CCtrIO(CCtrPmac *pCtrPmac )
{
	m_pCtrPmac = pCtrPmac;
}

CCtrIO::~CCtrIO()
{
	
}

/**
 *功能:打开对应IO口
 *参数:	uIndex--编号
 *其他:2013/11/19 By Jim Wen Ver1.0
**/
void CCtrIO::On(const UINT uIndex)
{
	m_pCtrPmac->SetShort('M', uIndex, 1);
}

/**
 *功能:关闭对应IO口
 *参数:	uIndex--编号
 *返回:无
 *其他:2013/11/19 By Jim Wen Ver1.0
**/
void CCtrIO::Off(const UINT uIndex)
{
	m_pCtrPmac->SetShort('M', uIndex, 0);
}

/**
 *功能:获得对应IO口状态
 *参数:	uIndex--编号
 *返回:TRUE--IO口打开
 *     FALSE--IO口关闭
 *其他:2013/11/19 By Jim Wen Ver1.0
**/
BOOL CCtrIO::GetState(const UINT uIndex)
{
	return m_pCtrPmac->GetShort('M', uIndex) == 0 ? FALSE : TRUE;
}
