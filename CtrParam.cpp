#include <stdafx.h>
#include <stdio.h>
#include "CtrHeader.h"
#include "CtrPmac.h"
#include "CtrParam.h"

CCtrParam::CCtrParam(CCtrPmac *pCtrPmac )
{
	m_pCtrPmac = pCtrPmac;
}

CCtrParam::~CCtrParam()
{
	
}

/**
 *功能:设置对应double型参数值
 *参数:	uIndex--编号
		dbValue--参数值
 *返回:无
 *其他:2013/11/19 By Jim Wen Ver1.0
**/
void CCtrParam::SetDouble( const UINT uIndex, const double dbValue)
{
	m_pCtrPmac->SetDouble('P', uIndex, dbValue);
}

/**
 *功能:获得对应double型参数值
 *参数:	uIndex--编号
 *返回:参数值
 *其他:2013/11/19 By Jim Wen Ver1.0
**/
double CCtrParam::GetDouble(const UINT uIndex)
{
	return m_pCtrPmac->GetDouble('P', uIndex);
}

/**
 *功能:设置对应long型参数值
 *参数:	uIndex--编号
		lnValue--参数值
 *返回:无
 *其他:2013/11/20 By Jim Wen Ver1.0
**/
void CCtrParam::SetLong(const UINT uIndex, const long lnValue )
{
	m_pCtrPmac->SetLong('P', uIndex, lnValue);
}

/**
 *功能:获得对应long型参数值
 *参数:	uIndex--编号
 *返回:参数值
 *其他:2013/11/20 By Jim Wen Ver1.0
**/
long CCtrParam::GetLong(const UINT uIndex)
{
	return m_pCtrPmac->GetLong('P', uIndex);
}
