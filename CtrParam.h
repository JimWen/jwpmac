#ifndef CTRPARAM_H_H_H
#define CTRPARAM_H_H_H

class CONTROL_DLL CCtrParam
{
public:
	CCtrParam(CCtrPmac *pCtrPmac);
	virtual ~CCtrParam();
	void CCtrParam::SetDouble( const UINT uIndex, const double dbValue);
	double GetDouble(const UINT uIndex);
	void SetLong(const UINT uIndex, const long lnValue);
	long GetLong(const UINT uIndex);
private:
	CCtrPmac *m_pCtrPmac;
};

#endif